from service import WordSet


class Generator:
    def __init__(self):
        self.dict = WordSet()
        self.dict.load_data()

    def generate(self, sent_n=5):
        i = sent_n
        string = ''
        while i:
            key = self.dict.random_key()
            word = self.dict.random_word(key)

            string += key.capitalize()
            while word != '. ':
                string += ' ' + word
                word = self.dict.random_word(word)
            string += word
            i -= 1

        return string

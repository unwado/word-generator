# word-generator

To start program enter `python run.py [options]`

Available options:

`--analyse [path]` - program scans selected file for text
`--generate [sentences-amount]` - program generates selected amount of sentences, basing on --analyse results
`--reset` - program forgets everything it ever knew


*Notification:* you need folder `suppl` for correct work

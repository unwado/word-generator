from sys import argv
from os.path import isfile, isdir
from os import remove, mkdir
from analysator import TextAnalyser
from generator import Generator
from config import ARCHIVE_FILE


if not isdir('suppl'):
    mkdir('suppl')

if argv[1] == '--generate':
    generator = Generator()
    print(generator.generate(int(argv[2])))

elif argv[1] == '--analyse' and isfile(argv[2]):
    analyser = TextAnalyser(argv[2])
    analyser.analyse()
    print('Analysis has been completed')
elif argv[1] == '--reset':
    remove(ARCHIVE_FILE)
    print('Archive file has been deleted')
else:
    print('Incorrect input, check README.md file')




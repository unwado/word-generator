from config import SENTENCE_END_CHAR, SPECIAL_SYMBOLS, ARCHIVE_FILE
from os.path import isfile
from service import WordSet


class TextAnalyser:
    def __init__(self, path_to_txt):
        self.path = path_to_txt

    def handle_text(self):
        f = open(self.path, 'r')
        raw_text = f.read()
        f.close()

        raw_text = raw_text.lower()
        raw_text = raw_text.translate({ord(c): ord('.') for c in SENTENCE_END_CHAR})
        raw_text = raw_text.translate({ord(c): None for c in SPECIAL_SYMBOLS})
        sentences = set(raw_text.split('.'))
        sentences.remove('')

        word_set = WordSet()
        for sentence in sentences:
            words = sentence.split(' ')
            for i in range(len(words) - 1):
                word_set.append(words[i], words[i+1])
            word_set.append(words[len(words)-1], None)
        return word_set

    def analyse(self):
        word_set = self.handle_text()
        if isfile(ARCHIVE_FILE):
            word_set.load_data()
        word_set.write_data()



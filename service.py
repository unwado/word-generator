from config import ARCHIVE_FILE
from random import randint


class WordSet:
    def __init__(self):
        self.set = dict()

    def append(self, key, value):
        if self.get(key) is None:
            if value is None:
                self.set.update({key: []})
            else:
                self.set.update({key: [value]})
        elif value not in self.get(key) and value is not None:
            self.get(key).append(value)

    def append_list(self, key, val_list):
        for val in val_list:
            self.append(key, val)

    def get(self, key):
        return self.set.get(key)

    def keys(self):
        return [*self.set]

    def random_key(self):
        return self.keys()[randint(0, len(self.keys()) - 1)]

    def random_word(self, key):
        length = len(self.get(key))
        if length == 1:
            return '. '
        return self.get(key)[randint(0, length - 1)]

    def load_data(self):
        f = open(ARCHIVE_FILE, 'r')
        raw_text = f.read()
        f.close()

        keyvals = raw_text[:-1].split(';')
        for keyval in keyvals:
            key, val = keyval.split(':')
            val_list = val.split(',')

            self.append_list(key, val_list)
        return self.set

    def write_data(self):
        string = ''
        for key, val in self.set.items():
            string += key + ':'
            for el in val:
                string += el + ','
            if self.get(key):
                string = string[:-1] + ';'
            else:
                string += ';'
        f = open(ARCHIVE_FILE, 'w')
        f.write(string)
        f.close()
